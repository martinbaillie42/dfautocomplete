;
/**
 * The Digital Fibre namespace. It is instantiated only if it does not already exist.
 * @type {Object}
 */
var df = df || {};

/**
 * Singleton module to route requests to collections or resources and return results.
 * @param  {Object}     $   Global jQuery library
 * @return {None} No value is returned.
 */
df.controller = (function( $ ) {

    /**
     * The first part of the URL used by all requests.
     * @type {String}
     */
    var baseUrl = '/api',

        /**
         * The data format of the collection
         * @type {String}
         */
        dataType = 'json';

    /**
     * [retrieveViaAjax description]
     * @param  {String}   url      [description]
     * @param  {Function} callback [description]
     * @return {None} No value is returned.
     */
    var retrieveViaAjax = function ( url, callback ) {
        $.ajax( url, {dataType: dataType} )
            .success( function( data ){
                callback( data );
            } );
    };

    /**
     * This method is run if the request verb passed to the action method is 'query'. It constructs a URL
     * and uses the retrieveViaAjax method to retrieve a collection. Once the Ajax request is complete it
     * calls the queryCallback function to filter the collection based on the value of the resource
     * parameter.
     * @param  {String}   verb       The action to be performed on the target collection or resource.
     * @param  {String}   collection The group of resources that are requested.
     * @param  {String}   resource   The resource that is requested.
     * @param  {Function} callback   The function to be run after the ajax request is complete.
     * @return {None} No value is returned.
     */
    var query = function( verb, collection, resource, callback ) {

        /**
         * URL for the requested collection.
         * @type {[type]}
         */
        var url = baseUrl + '/' + collection + '/' + verb + '/' + resource;

        /**
         * Callback function to filter the collection based on the value of the resource
         * parameter.
         * @param  {Object} data JSON object containing collection of resources.
         * @return {None} No value is returned.
         */
        var queryCallback = function( data ) {
            var result = [];
            $.each( data, function(i, value) {
                if( resource.length > 0 && value.name.toLowerCase().search( resource ) !== -1 ) {
                    result.push( value );
                }
            } );

            callback( result );
        }

        retrieveViaAjax( url, queryCallback );
    };
    /**
     * This method stub is run if the request verb passed to the action method is 'get'.
     * @param  {String}   verb       The action to be performed on the target collection or resource.
     * @param  {String}   collection The group of resources that are requested.
     * @param  {String}   resource   The resource that is requested.
     * @param  {Function} callback   The function to be run after the ajax request is complete.
     * @return {None} No value is returned.
     */
    var get = function( verb, collection, resource, callback ) {
        // ...
    }

    return {

        /**
         * Route requests based on the verb
         * @param  {String}   verb       The action to be performed on the target collection or resource.
         * @param  {String}   collection The group of resources that are requested.
         * @param  {String}   resource   The resource that is requested.
         * @param  {Function} callback   The function to be run after the ajax request is complete.
         * @return {None} No value is returned.
         */
        action: function( verb, collection, resource, callback ) {

            if( verb === 'query' ) {
                query( verb, collection, resource, callback );
            } else if ( verb === 'get'  ) {
                get( verb, collection, resource, callback );
            } // ...

        }

    };
})( jQuery );