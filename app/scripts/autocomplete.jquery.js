;
/**
 * Autocomplete jQuery plugin that performs a search against a query against the collection specified. The results are displayed
 * for the user to select.
 * @param  {Object}     $               Global jQuery library
 * @param  {Object}     window          Global window object. Passing window through as local variables
 *                                      (slightly) improves the resolution process and enables more
 *                                      efficient minification.
 * @param  {Object}     document        Global document object. Passing document through as local variables
 *                                      (slightly) improves the resolution process and enables more efficient minification.
 * @param  {undefined}  undefined       In ECMAScipt undefined is mutable. Defining it without assigning a value ensures
 *                                      the value is undefined.
 * @return {None}                       No value is returned. jQuery objects should be chainable so this will need to be changed.
 */
(function ( $, window, document, undefined ) {

    /**
     * pluginName   The name assigned to the plugin.
     * @type        {String}
     */
    var pluginName = "autocomplete",

        /**
         * The usual values that are used by the plugin. These can be overwritten when the plugin
         * is instantiated by assigning values to the options object.
         * @type    {Object}
         */
        defaults = {

            /**
             * The minimum number of characters that need to be entered before a resource
             * collection is queried.
             * @type                {Number}
             */
            autocompleteOnChar: 3,

            /**
             * The controller object is a dependency that has to be passed in when the plugin is
             * instantiated. The controller is responsible for routing the resource collection
             * request to the correct location. the controller is explicitly declared as a dependency
             * on instantiation to enable it to be more readily identifiable to future developers.
             * @type        {Object}
             */
            controller: {},

            /**
             * The class or id of the element used to display the returned collection.
             * @type                {String}
             */
            autocompleteDropdown: '.autocomplete-dropdown',

            /**
             * The IDs of any returned records that the user has already selected are stored here.
             * The '|' is the separator.
             * @type            {String}
             */
            recordsSelected: '|'

        };

    /**
     * Constructor for the plugin
     * @param {Object} element The element the plugin is instantiated against.
     * @param {Object} options the options object, as defined in the instantiating function call.
     */
    function Plugin( element, options ) {

        /**
         * this.element is the element the plugin is instantiated against.
         * @type {object}
         */
        this.element = element;

        /**
         * this.options is created by combining the defaults and the options objects. Any values
         * that exist in options will overwrite those in defaults.
         * @type {Object}
         */
        this.options = $.extend( {}, defaults, options) ;

        /**
         * this._defaults are stored as a private instance variable for reference.
         * @type {Object}
         */
        this._defaults = defaults;

        /**
         * this._name is an instance variable of the static pluginName,
         * @type {[type]}
         */
        this._name = pluginName;

        this.init();
    }

    /**
     * The main method for the plugin. When a user enters characters into a text input the
     * plugin retrieves a collection of resources via the controller, displays them to the user
     * and adds them to to form when the user selects them.
     * @return {None} No value is returned.
     */
    Plugin.prototype.init = function () {

        /**
         * Assign the value of this to that to enable the value of this to be accessed in different
         * contexts.
         * @type {Object}
         */
        var that = this,

            /**
             * Set the verb to be used in the URL by the Controller.
             * @type {String}
             */
            verb = $(that.element).data('api-verb'),

            /**
             * Set the collection to be used in the URL by the Controller.
             * @type {String}
             */
            collection = $(that.element).data('api-collection');

        /**
         * A callback that is passed to the controller action method. This is required due the action method using an
         * asynchronous ajax request. The template used to display the results is hardcoded in the callback. For proper
         * separation of concerns the template should be passed in as an option. For full flexibility the below method should
         * also be passed in an option.
         * @param  {Object} result  JSON object containing the results of the query.
         * @return {None}           No value is returned. jQuery objects should be chainable so this will need to be changed.
         */
        var displayResults = function( result ) {

            /**
             * The number of records returned.
             * @type {Integer}
             */
            var resultLength = result.length,

                /**
                 * The HTML template to use to display the results. This should be refactored into template.js
                 * @type {String}
                 */
                resultsTemplate = '<ul class="list-unstyled"><li><a href="{{1}}">{{2}}</a></li></ul>',

                /**
                 * displayResults is outputted to the DOM once the template is constructed.
                 * @type {String}
                 */
                displayResults = '';

            /*
             * Iterate over the result object and construct the displayResults string to append to
             * the autocompleteDropdown element
             */
            $.each(result, function(i, value) {

                if (i < resultLength ) {
                    resultsTemplate = resultsTemplate.replace('</ul>', '');
                }
                if ( i > 0 ){
                    resultsTemplate = resultsTemplate.replace('<ul class="list-unstyled">', '');
                }

                if ( that.options.recordsSelected.indexOf( '|' + value.id + '|' ) !== -1 ) {
                    displayResults += resultsTemplate.replace('<a href="{{1}}">{{2}}</a>', '{{2}}')
                                                    .replace('{{1}}', value.id).replace('{{2}}', value.name);
                } else {
                    displayResults += resultsTemplate.replace('{{1}}', value.id).replace('{{2}}', value.name);
                }

            } );

            $(that.options.autocompleteDropdown, that.element)
                .html('')
                .append(displayResults);

        };


        /*
         * For every character entered into the text input test if the total number of characters
         * is equal to or greater than the value of autocompleteOnChar. If yes then call the
         * controller.action method to query the database.
         */
         */
        $( 'input', that.element ).on( 'keyup', function() {
            var resource = '';
            if ( $( this ).val().length >= that.options.autocompleteOnChar || that.options.autocompleteOnChar === 1) {
                resource = $('input', that.element).val();
                that.options.controller.action( verb, collection, resource, displayResults );
            }
        } );


        /*
         * When a user clicks on a returned resource that resource is added to the form, and
         * is greyed out and the a tag removed in the dropdown.
         */
        $(that.element).on( 'click', 'a', function ( event ) {
            event.preventDefault;

            var userTemplate = '<div class="input-group input-group-lg" >'
                                    + '<label for="user{{1}}">User {{1}}</label>'
                                    + '<input type="text" class="form-control" id="user{{1}}" name="user{{1}}" autocomplete="off" value="{{2}}">'
                                + '</div>'

            var name = $(this).html();
            var id = $(this).attr('href');
            var parent = $(this).parent();

            userTemplate = userTemplate
                            .replace(/\{\{1\}\}/g, id)
                            .replace(/\{\{2\}\}/g, name);

            $(that.element).parent('form').append(userTemplate);

            $(this).remove();
            $(parent).html(name);

            that.options.recordsSelected += $(this).attr('href') + '|';

            return false;
        });

    };

    // A lightweight plugin wrapper around the constructor. it
    // prevents against multiple instantiations on the same element.
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if ( !$.data(this, "plugin_" + pluginName )) {
                $.data( this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    }

})( jQuery, window, document);