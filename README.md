# DF Autocomplete #

### Getting Started ###

* Fetch the repository or download the zip file.
* Install dependencies: `npm install --global yo gulp-cli bower`.
* Run `gulp serve`. This will open the application in your browser and watch for any changes to files.
* The majority files of interest are in the 'app' directory.

### Notes ###
Due to time limitations:

* Bower has been used instead of RequireJS;
* the templates used by autocomplete.jquery are hardcoded into the plugin instead of being defined in a separate object;
* no tests;
* there is no non-Javascript fallback for the query form submission.